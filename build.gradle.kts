import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.3.3.RELEASE"
	id("io.spring.dependency-management") version "1.0.10.RELEASE"
	kotlin("jvm") version "1.3.72"
	kotlin("plugin.spring") version "1.3.72"
}

group = "com.poc"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenLocal()
	mavenCentral()

}

object Versions {
	const val springCloud = "Hoxton.RELEASE"
	const val logback = "6.4"
	const val springMockK = "2.0.0"
	const val jwt = "3.10.3"
	const val googleApiClient = "1.30.9"
	const val r2dbc = "Arabba-SR5"
	const val bouncyCastle = "1.64"
	const val springDataR2dbc = "1.1.1.RELEASE"
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-webflux")


	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
	implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")

	implementation("net.logstash.logback:logstash-logback-encoder:${Versions.logback}")

	implementation("com.auth0:java-jwt:${Versions.jwt}")
	implementation("com.google.api-client:google-api-client:${Versions.googleApiClient}")
	implementation("com.jayway.jsonpath:json-path")
	implementation("org.bouncycastle:bcprov-jdk15on:${Versions.bouncyCastle}")
	implementation("org.springframework.boot:spring-boot-starter-data-redis-reactive")

	implementation("org.jetbrains.kotlin:kotlin-test-junit:1.4.0")

	implementation("org.springframework.data:spring-data-r2dbc:${Versions.springDataR2dbc}")
	implementation("io.r2dbc:r2dbc-pool")
	implementation("io.r2dbc:r2dbc-h2")
	implementation("io.r2dbc:r2dbc-mssql")

	annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}
	testImplementation("io.projectreactor:reactor-test")
	testImplementation("com.ninja-squad:springmockk:${Versions.springMockK}")
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.register("bootRunDev") {
	group = "application"
	description = "Runs this project as a Spring Boot application with the dev profile"
	doFirst {
		tasks.bootRun.configure {
			systemProperty("spring.profiles.active", "dev")
		}
	}
	finalizedBy("bootRun")
}
