package com.poc.shoppingonline.utils

import com.fasterxml.jackson.databind.node.ObjectNode
import org.springframework.data.redis.core.ReactiveRedisTemplate
import org.springframework.data.redis.core.getAndAwait
import org.springframework.data.redis.core.setAndAwait

class RedisUtils (
	private val redisStringTemplate: ReactiveRedisTemplate<String, String>
){
	suspend fun getData(key: String) = redisStringTemplate.opsForValue().getAndAwait(key)

	suspend fun putDataString(
		key: String,
		data: String
	) = redisStringTemplate.opsForValue().setAndAwait(key, data)
}