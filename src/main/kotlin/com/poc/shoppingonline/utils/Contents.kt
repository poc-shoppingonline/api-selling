package com.poc.shoppingonline.utils


object Constants {
	const val SCHEMA_SQL_PATH = "db/schema.sql"
	const val DATA_SQL_PATH = "db/data.sql"


	const val HEADER_REQ_ID = "header_reqID"
	const val HEADER_REQ_DTM = "header_reqDtm"
	const val HEADER_SERVICE = "header_service"
	const val HEADER_TXN_REF_ID = "header_txnRefID"
	const val ERR_MSG_ATTRIBUTES = "message"
	const val API_SUCCESS_CODE = "0000"
	const val API_FAILURE_CODE = "9999"
	const val ENCOURAGEMENT_SOF_TYPE = "EM"

}
