package com.poc.shoppingonline

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SellingApplication

fun main(args: Array<String>) {
	runApplication<SellingApplication>(*args)
}
