package com.poc.shoppingonline.features.api.selling

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.coRouter


@Configuration
class SellingRouter {
	@Bean
	fun getFlowRouter(sellingHandler: SellingHandler) = coRouter {
		accept(MediaType.APPLICATION_JSON).nest {
			POST("/art", sellingHandler::getItem)
		}
	}

}