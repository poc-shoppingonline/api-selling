package com.poc.shoppingonline.features.api.selling

import com.poc.shoppingonline.data.entities.ProductResponse
import com.poc.shoppingonline.dto.TransferRequest
import com.poc.shoppingonline.features.api.selling.service.SellingService
import com.poc.shoppingonline.utils.transferResponseSuccess
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.awaitBody


import org.springframework.stereotype.Component


@Component
class SellingHandler(private val sellingService: SellingService) {
	suspend fun getItem(request: ServerRequest) = 
		request.awaitBody<TransferRequest<ProductResponse>>().let {
			println("OK")
			transferResponseSuccess(
				it.headerRequest,
				sellingService.insertProduct(it.content)
			)
		}


}