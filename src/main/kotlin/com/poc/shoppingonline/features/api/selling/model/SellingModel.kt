package com.poc.shoppingonline.features.api.selling.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.time.temporal.TemporalAmount

@JsonIgnoreProperties(ignoreUnknown = true)
data class SellingRequest(
	val productId: String,
	val price: Int,
	val amount: Int,
	val detail:String
)