package com.poc.shoppingonline.features.api.selling.service

import com.poc.shoppingonline.data.entities.ProductResponse
import com.poc.shoppingonline.features.api.selling.model.SellingRequest
import org.springframework.data.r2dbc.core.DatabaseClient
import org.springframework.data.r2dbc.core.await
import org.springframework.stereotype.Service


@Service
class SellingService(
	private val databaseClient: DatabaseClient
	) {


	suspend fun checkData(content: SellingRequest){

	}

	internal suspend fun insertProduct(request: ProductResponse) =
		databaseClient.insert()
			.into(ProductResponse::class.java)
			.using(request)
			.await()
}