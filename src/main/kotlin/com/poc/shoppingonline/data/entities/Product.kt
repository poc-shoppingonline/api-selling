package com.poc.shoppingonline.data.entities

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("PRODUCT")
data class ProductResponse(
	@Id val productId: String,
	val price: Int,
	val amount: Int,
	val detail:String
)